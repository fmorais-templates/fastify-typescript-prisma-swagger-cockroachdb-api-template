export const ERROR_UTILS = {
  INVALID_NODE_ENV: (nodeEnv: string) => (new Error(`'${nodeEnv}' isn't a valid env value to this API`))
}