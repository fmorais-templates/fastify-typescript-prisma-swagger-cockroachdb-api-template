import { Static, Type } from '@sinclair/typebox';

export const CONTENT_TYPES = {
  APPLICATION_JSON: 'application/json',
  PLAIN_TEXT: 'plain/text',
};

export const REGEX_CONSTS = {
  SEMVER:
    /^((\d+\.\d+\.\d+(?:-([\w-]+(?:\.[\w-]+)*))?)(?:\+([\w-]+(?:\.[\w-]+)*))?)$/,
};

export const TYPES_CONSTS = {
  UF_TYPE: Type.Object({ nome: Type.String(), sigla: Type.String() }),
  STATUS_TYPE: Type.Object({ version: Type.String() }),
  EMPTY_DATA_SAMPLE: Type.Object({ id: Type.String() }),
};

export type EmptyDataSampleType = Static<typeof TYPES_CONSTS.EMPTY_DATA_SAMPLE>;
