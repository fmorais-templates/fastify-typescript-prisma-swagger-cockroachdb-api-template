export function defaultSystemErrorHandler(err?: Error) {
  if (err) throw err;
  throw new Error('[000]-Unexpected')
}