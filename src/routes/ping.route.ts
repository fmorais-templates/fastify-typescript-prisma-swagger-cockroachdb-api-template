import { Type } from '@sinclair/typebox';
import { Plugin } from '../lib/createServer.lib';
import { CONTENT_TYPES } from '../utils/constants.util';

// eslint-disable-next-line @typescript-eslint/require-await
const ping: Plugin = async (server) => {
  server.get(
    '/ping',
    {
      schema: {
        produces: [CONTENT_TYPES.PLAIN_TEXT],
        response: {
          200: Type.String(),
        },
      },
    },
    () => {
      return 'pong';
    },
  );
};

export default ping;
