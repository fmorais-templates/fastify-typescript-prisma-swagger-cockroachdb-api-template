import { TypeBoxTypeProvider } from '@fastify/type-provider-typebox';
import { Type } from '@sinclair/typebox';
import Fastify from 'fastify';
import { Plugin } from '../lib/createServer.lib';
import {
  CONTENT_TYPES,
  EmptyDataSampleType,
  TYPES_CONSTS,
} from '../utils/constants.util';

Fastify().withTypeProvider<TypeBoxTypeProvider>();

// eslint-disable-next-line @typescript-eslint/require-await
const emptyDataSample: Plugin = async (server) => {
  server.get<{ Reply: EmptyDataSampleType[] }>(
    '/empty-data-sample',
    {
      schema: {
        produces: [CONTENT_TYPES.APPLICATION_JSON],
        response: {
          200: Type.Array(TYPES_CONSTS.EMPTY_DATA_SAMPLE),
        },
      },
    },
    () => { 
      return server.prisma.emptyDataSample.findMany();
    },
  );

  server.get<{ Reply: EmptyDataSampleType | null; Params: { id: string } }>(
    '/empty-data-sample/:id',
    {
      schema: {
        produces: [CONTENT_TYPES.APPLICATION_JSON],
        response: {
          200: TYPES_CONSTS.EMPTY_DATA_SAMPLE,
        },
        params: {
          id: Type.String()
        }
      },
    },
    (req) => {
      const { id } = req.params;
      return server.prisma.emptyDataSample.findUnique({ where: { id: id } });
    },
  );

  server.post<{
    Reply: EmptyDataSampleType | null;
    Body: { emptyData?: unknown };
  }>(
    '/empty-data-sample',
    {
      schema: {
        produces: [CONTENT_TYPES.APPLICATION_JSON],
        response: {
          200: TYPES_CONSTS.EMPTY_DATA_SAMPLE,
        },
        body: {
          emptyData: Type.Any()
        }
      },
    },
    async (req) => {
      const { emptyData } = req.body;
      return server.prisma.emptyDataSample.create({ data: emptyData as object });
    },
  );
};

export default emptyDataSample;
