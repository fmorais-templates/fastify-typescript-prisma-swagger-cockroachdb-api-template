import packageJSON from '../../package.json';
import { Plugin } from '../lib/createServer.lib';
import { CONTENT_TYPES, TYPES_CONSTS } from './../utils/constants.util';

// eslint-disable-next-line @typescript-eslint/require-await
const status: Plugin = async (server) => {
  server.get(
    '/status',
    {
      schema: {
        produces: [CONTENT_TYPES.APPLICATION_JSON],
        response: {
          200: TYPES_CONSTS.STATUS_TYPE,
        },
      },
    },
    () => {
      return {
        version: packageJSON.version,
      };
    },
  );
};

export default status;
