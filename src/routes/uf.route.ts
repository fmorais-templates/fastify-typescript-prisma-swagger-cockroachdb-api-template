import { Type } from '@sinclair/typebox';
import ufs from '../../assets/json/ufs.json';
import { Plugin } from '../lib/createServer.lib';
import { CONTENT_TYPES, TYPES_CONSTS } from '../utils/constants.util';

// eslint-disable-next-line @typescript-eslint/require-await
const uf: Plugin = async (server) => {
  server.get(
    '/ufs',
    {
      schema: {
        produces: [CONTENT_TYPES.APPLICATION_JSON],
        response: {
          200: Type.Array(TYPES_CONSTS.UF_TYPE),
        },
      },
    },
    () => {
      return ufs;
    },
  );
};

export default uf;
