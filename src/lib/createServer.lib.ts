import { FastifyPluginAsync, FastifyRegisterOptions } from 'fastify';
import { Config } from '../config/server.config';

import autoload from '@fastify/autoload';
import fastifyStatic from '@fastify/static';
import fastifySwagger, { FastifyDynamicSwaggerOptions } from '@fastify/swagger';
import path from 'path';

import packageJSON from '../../package.json';
import fastifySwaggerUi, { FastifySwaggerUiOptions } from '@fastify/swagger-ui';
import prismaPlugin from '../plugins/prisma';

export interface PluginOpts {
  config: Config;
}

export type Plugin = FastifyPluginAsync<PluginOpts>;

const fastifySwaggerOptions: FastifyRegisterOptions<FastifyDynamicSwaggerOptions> =
  {
    swagger: {
      info: {
        title: 'Basic Fastity TS Template',
        description: 'Swagger API for Basic Fastify TS template',
        version: packageJSON.version,
      },
      schemes: ['http', 'https'],
      consumes: ['application/json'],
      produces: ['application/json', 'text/plain'],
    },
  };

const fastifySwaggerUiOptions: FastifyRegisterOptions<FastifySwaggerUiOptions> =
  {
    routePrefix: '/api-docs',
  };

const createServer: Plugin = async (server, opts) => {
  await server.register(fastifySwagger, fastifySwaggerOptions);
  await server.register(fastifySwaggerUi, fastifySwaggerUiOptions);
  await server.register(prismaPlugin);

  void server.register(fastifyStatic, {
    root: path.join(process.cwd(), '/public'),
    prefixAvoidTrailingSlash: true,
  });

  void server.register(autoload, {
    dir: path.join(__dirname, '../routes'),
    ignorePattern: /.test.ts/,
    dirNameRoutePrefix: false,
    options: {
      prefix: '/api',
      config: opts.config,
    },
  });
};

export default createServer;
