import envVar from 'env-var';
import { defaultSystemErrorHandler } from '../utils/base.util';
import { ERROR_UTILS } from '../utils/erros.util';

export function getConfig() {
  const env = {
    SERVER_HOST: envVar.get('SERVER_HOST').default('127.0.0.1').asString(),
    SERVER_PORT: envVar.get('SERVER_PORT').default('3000').asPortNumber(),
    NODE_ENV: envVar.get('NODE_ENV').default('production').asString(),
  };

  const possiblesNodeEnv = [
    'production',
    'prod',
    'development',
    'dev',
    'testing',
    'test',
  ];
 
  if (possiblesNodeEnv.indexOf(env.NODE_ENV) < 0) {
    defaultSystemErrorHandler(ERROR_UTILS.INVALID_NODE_ENV(env.NODE_ENV));
  }

  const isProduction = env.NODE_ENV === 'production' || env.NODE_ENV === 'prod';

  return {
    isProduction,
    fastify: {
      host: env.SERVER_HOST,
      port: env.SERVER_PORT,
    },
    fastifyInit: {
      logger: true,
    },
  };
}

export function getTestingConfig(): Config {
  return {
    isProduction: false,
    fastify: {
      host: '0.0.0.0',
      port: 3574,
    },
    fastifyInit: {
      logger: false,
    },
  };
}

export type Config = ReturnType<typeof getConfig>;
