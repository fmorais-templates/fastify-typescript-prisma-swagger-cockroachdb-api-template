import { TypeBoxTypeProvider } from '@fastify/type-provider-typebox';
import * as dotenv from 'dotenv';
import fastify from 'fastify';
import { getConfig } from './config/server.config';
import createServer from './lib/createServer.lib';
import { defaultSystemErrorHandler } from './utils/base.util';

dotenv.config();

const serverConfig = getConfig();

const server = fastify(
  serverConfig.fastifyInit,
).withTypeProvider<TypeBoxTypeProvider>();

async function run() {
  process.on('unhandledRejection', (err: Error) => {
    defaultSystemErrorHandler(err);
    process.exit(1);
  });

  server.setNotFoundHandler((_, reply) => {
    void reply.sendFile('404.html');
  });

  await server.register(createServer, { config: serverConfig });

  await server.listen(serverConfig.fastify);
}

void run();

export default run;