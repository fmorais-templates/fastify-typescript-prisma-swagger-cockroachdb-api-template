-- CreateTable
CREATE TABLE "EmptyDataSample" (
    "id" UUID NOT NULL DEFAULT gen_random_uuid(),

    CONSTRAINT "EmptyDataSample_pkey" PRIMARY KEY ("id")
);
