import { describe, expect } from '@jest/globals';
import Fastify from 'fastify';
import { getTestingConfig } from '../../src/config/server.config';

import uf from '../../src/routes/uf.route';

describe('uf should return 27 uf', () => {
  let config: any;
  let server: any;

  beforeEach(() => {
    config = getTestingConfig(); 
    server = Fastify(config.fastifyInit);
  });

  afterEach(async () => {
    await server.close();
  });

  test('ufs.get',async () => {
    await server.register(uf, { config }).ready();
  
    const res = await server.inject({
      method: 'GET',
      url: '/ufs',
    });
  
    expect(res.statusCode).toBe(200);
    expect(JSON.parse(res.payload).length).toBe(27);
  })
});
