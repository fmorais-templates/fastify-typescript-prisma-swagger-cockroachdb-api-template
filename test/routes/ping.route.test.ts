import { describe, expect } from '@jest/globals';
import { FastifyInstance } from 'fastify';
import { getTestingConfig } from '../../src/config/server.config';

import ping from '../../src/routes/ping.route';

describe('ping should return pong', () => {
	let config: any;
	let server: FastifyInstance;

	beforeEach(()=>{
		config = getTestingConfig()
		server = require('fastify')(config.fastifyInit) as FastifyInstance
	})

	afterEach(async()=>{
		await server.close()
	})

	test('ping.route', async ()=>{
		await server.register(ping, { config }).ready()
	
		const res = await server.inject({
			method: 'GET',
			url: '/ping'
		})
	
		expect(res.statusCode).toBe(200)
		expect(res.payload).toBe('pong')
	})
})