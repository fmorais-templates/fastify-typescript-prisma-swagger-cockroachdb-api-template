import { describe, expect } from '@jest/globals';
import { FastifyInstance } from 'fastify';
import { getTestingConfig } from '../../src/config/server.config';
import { REGEX_CONSTS } from '../../src/utils/constants.util';

import status from '../../src/routes/status.route';

describe('status.route', () => {
  let config: any;
  let server: FastifyInstance;

  beforeEach(() => {
    config = getTestingConfig();
    server = require('fastify')(config.fastifyInit) as FastifyInstance;
  });

  afterEach(async () => {
    await server.close();
  });

  test('get status', async () => {
    await server.register(status, { config }).ready();

    const res = await server.inject({
      method: 'GET',
      url: '/status',
    });

    expect(res.statusCode).toBe(200);
    expect(JSON.parse(res.payload).version).toMatch(REGEX_CONSTS.SEMVER);

    await server.close();
  });
});
