import { describe, expect, test } from '@jest/globals';
import { getTestingConfig } from '../../src/config/server.config';
import prismaPlugin from '../../src/plugins/prisma';

import emptyDataSample from '../../src/routes/emptyDataSample.route';
import Fastify from 'fastify';
import { TypeBoxTypeProvider } from '@fastify/type-provider-typebox';

describe('emptyDataSample list', () => {
  let config: any;
  let server: any;

  beforeEach(() => {
    config = getTestingConfig();
    server = Fastify(config.fastifyInit).withTypeProvider<TypeBoxTypeProvider>();
  });
  
  afterEach(async () => {
    await server.close();
  });
  
  test('get.list and get.byID', async () => {
    await server.register(prismaPlugin);
    await server.register(emptyDataSample, { config }).ready();

    const res = await server.inject({
      method: 'GET',
      url: '/empty-data-sample',
    });

    const body: any = JSON.parse(res.body);

    const { id } = body[0];

    expect(res.statusCode).toBe(200);

    const ret = await server.inject({
      method: 'GET',
      url: `/empty-data-sample/${id}`,
    });

    expect(ret.statusCode).toBe(200);

    await server.close();
  });

  test('post', async () => {
    await server.register(prismaPlugin);
    await server.register(emptyDataSample, { config }).ready();

    const res = await server.inject({
      method: 'post',
      url: '/empty-data-sample',
      payload: { emptyData: {} },
    });

    expect(res).toBeTruthy(); // should be => expect(res.statusCode).toBe(200);

    await server.close();
  });
});
