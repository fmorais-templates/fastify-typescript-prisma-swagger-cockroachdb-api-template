import { describe, expect, test } from '@jest/globals';
import { defaultSystemErrorHandler } from '../../src/utils/base.util';

describe('base.util', () => {
  test('defaultSystemErrorHandler',async () => {
    try {
      defaultSystemErrorHandler()
    } catch (error) {
      expect(error).toBeInstanceOf(Error)
    }
  })
});