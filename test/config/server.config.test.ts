import { describe, expect } from '@jest/globals';
import { getConfig, getTestingConfig } from '../../src/config/server.config';

describe('server.config', () => {
  process.env['NODE_ENV'] = 'prod';
  const confProduction = getConfig();
  expect(confProduction.isProduction).toBe(true);
});

describe('server.config prod', () => {
  process.env['NODE_ENV'] = 'prod';
  const confProd = getConfig();
  expect(confProd.isProduction).toBe(true);
});

describe('server.config test', () => {
  const confTesting = getTestingConfig();
  expect(confTesting.isProduction).toBe(false);
});

describe('server.config com-erro', () => {
  const nodeEnvStr = 'com-erro';
  process.env['NODE_ENV'] = nodeEnvStr;
  test('getConfig com-erro', async () => {
    try {
      getConfig()
    } catch (error) {
      expect(error).toBeInstanceOf(Error)
    }
  })
});
