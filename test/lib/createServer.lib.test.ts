import { describe, expect } from '@jest/globals';
import Fastify from 'fastify';
import { getTestingConfig } from '../../src/config/server.config';

import createServer from '../../src/lib/createServer.lib';

describe('createServer.lib', ()=>{
  let config: any;
	let server: any;

	beforeEach(()=>{
		config = getTestingConfig()
		server = Fastify(config.fastifyInit)
	})

	afterEach(async()=>{
		await server.close()
	})

  test('register', async ()=>{
    await server.register(createServer, {config}).ready();
    expect(1).toBeTruthy();
  })

})